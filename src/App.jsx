import "./App.css";
import Routers from "./Routes/Routers";
import CartContextProvider from "./context/CartContext/CartContextProvider";
import ContextProvider from "./context/ProductContext/ContextProvider";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <ContextProvider>
      <CartContextProvider>
        <Header />
        <Routers />
        <Footer />
      </CartContextProvider>
    </ContextProvider>
  );
}

export default App;
