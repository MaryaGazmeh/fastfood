import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { HiShoppingCart } from "react-icons/hi2";
import { isCart } from "../Utils/util";
import { CartContext } from "../context/CartContext/CartContextProvider";
import { quantity } from "../Utils/util";

const Card= ({ product }) => {
  const { state, dispatch } = useContext(CartContext);

  return (
    <div className=" w-auto bg-white shadow rounded hover:shadow-2xl">
      {" "}
      <img
        src={product.imageUrl}
        className="h-48 w-full flex flex-col justify-between p-4 bg-cover bg-center"
      />
      <div className="p-4 flex flex-col items-center">
        {" "}
        <h1 className="text-gray-600 font-bold text-center mt-1">
          {product.name}{" "}
        </h1>{" "}
        <p className="text-center text-green-600 mt-1">
          قیمت: {product.price.toLocaleString()} تومان
        </p>{" "}
        <p className="text-center text-gray-800 mt-1 font-light text-xs h-10">
          {product.ingredients}
        </p>
        <Link
          to={`/products/${product.id}`}
          className=" hover:text-green-500 px-2 py-1 rounded-md w-full text-xs font-bold text-right"
        >
          {" "}
          جزئیات محصول
        </Link>
        {isCart(state, product.id) ? (
          <div className="inline-flex items-center mt-2">
            <button
              onClick={() => dispatch({ type: "INCREASE", payload: product })}
              className="bg-white rounded-r border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
            >
              {" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-4"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                {" "}
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M12 4v16m8-8H4"
                />{" "}
              </svg>{" "}
            </button>
            <div className="bg-gray-100 border-t border-b border-gray-100 text-gray-600 hover:bg-gray-100 inline-flex items-center px-4 py-1 select-none">
              {" "}
              {quantity(state, product.id)}
            </div>{" "}
            {/* Minus button */}
            {quantity(state, product.id) > 1 && (
              <button
                onClick={() => dispatch({ type: "DECREASE", payload: product })}
                className="bg-white rounded-l border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-4"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  {" "}
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M20 12H4"
                  />{" "}
                </svg>{" "}
              </button>
            )}
            {quantity(state, product.id) === 1 && (
              <button
                onClick={() =>
                  dispatch({ type: "REMOVE_ITEM", payload: product })
                }
                className="bg-white rounded-l border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-4"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  {" "}
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M20 12H4"
                  />{" "}
                </svg>{" "}
              </button>
            )}
          </div>
        ) : (
          <button
            onClick={() => dispatch({ type: "ADD_ITEM", payload: product })}
            className="py-2 px-4 bg-green-500 text-white rounded hover:bg-green-600 active:bg-green-800 disabled:opacity-50 mt-4 w-full flex items-center justify-center"
          >
            افزودن به سبد خرید{" "}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 ml-2"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              {" "}
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
              />{" "}
            </svg>{" "}
          </button>
        )}
      </div>
    </div>
  );
};

export default Card;
