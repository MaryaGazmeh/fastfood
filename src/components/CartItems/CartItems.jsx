import React, { useContext } from "react";
import { CartContext } from "../../context/CartContext/CartContextProvider";
import { BsTrash } from "react-icons/bs";
import { BsTrashFill } from "react-icons/bs";

const CartItems = ({ data }) => {
  const { dispatch } = useContext(CartContext);

  return (
    <div className="flex justify-start w-[40%] text-sm md:text-lg">
      <div className=" border border-lime-200 m-2 p-7 rounded-md hover:shadow-xl">
        <img src={data.imageUrl} alt={data.name} className="w-full" />
        <div className="m-4">
          <p className="text-lg pb-2">{data.name}</p>
          <p>تومان: {data.price.toLocaleString()} قیمت</p>
          <p>تعداد: {data.quantity}</p>
        </div>
        <div className="inline-flex items-center mt-2">
          <button
            onClick={() => dispatch({ type: "INCREASE", payload: data })}
            className="bg-white rounded-r border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-4"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M12 4v16m8-8H4"
              />
            </svg>
          </button>
          <div className="bg-gray-100 border-t border-b border-gray-100 text-gray-600 hover:bg-gray-100 inline-flex items-center px-4 py-1 select-none">
            {data.quantity}
          </div>
          {data.quantity > 1 ? (
            <button
              onClick={() => dispatch({ type: "DECREASE", payload: data })}
              className="bg-white rounded-l border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-4"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M20 12H4"
                />
              </svg>
            </button>
          ) : (
            <button
              onClick={() => dispatch({ type: "REMOVE_ITEM", payload: data })}
              className="bg-white rounded-l border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
            >
              <BsTrashFill size={23} />
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default CartItems;