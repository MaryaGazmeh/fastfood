import React, { useContext } from "react";
import { CartContext } from "../../context/CartContext/CartContextProvider";
import CartItems from "../CartItems/CartItems";
import { Link } from "react-router-dom";

const ShopCart = () => {
  const { state, dispatch } = useContext(CartContext);
  return (
    <div className=" m-10 p-5">
      {state.selectedItems.length === 0 && (
        <div className="flex justify-between p-2 rounded-lg w-2/3">
          <p className=" text-2xl p-1.5">سبد خرید شما در حال حاضر خالی است.</p>
          <Link to='/' className=" bg-red-400 p-1 rounded-lg text-white"><span>بازگشت به فروشگاه</span></Link>
        </div>
      )}
      {state.selectedItems.map((item) => (
        <CartItems key={item.id} data={item} />
      ))}
    </div>
  );
};

export default ShopCart;
