import React, { useContext } from 'react'
import Product from '../Product/Product'
import { productContext } from '../../context/ProductContext/ContextProvider'

const AllProducts = () => {
    const {products , searchItems} = useContext(productContext)
  return (
    <div className="container mx-auto my-8">
    <div className="grid s:grid-cols-2 grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
      {products.map((product) => (
        <Product key={product.id} product={product} />
      ))}
    </div>
  </div>
  )
}

export default AllProducts