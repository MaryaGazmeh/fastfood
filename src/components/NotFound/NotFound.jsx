import React from "react";
import { Link } from "react-router-dom";
import pic from "../../assets/images/404.png";

const NotFound = () => {
  return (
    <div className="flex flex-col items-center justify-center p-4 md:p-8">
      <img className="w-80 md:w-96" src={pic} alt="Not Found" />
      <p className="text-xl sm:text-3xl font-bold my-4">
        محصولی یافت نشد
      </p>
      <Link
        to="/"
        className="bg-blue-500 hover:bg-blue-600 rounded-md text-white px-4 py-2"
      >
        بازگشت به فروشگاه
      </Link>
    </div>
  );
};

export default NotFound;
