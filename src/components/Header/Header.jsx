import React, { useState, useEffect, useContext } from "react";
import { FiShoppingCart } from "react-icons/fi";
import { VscAccount } from "react-icons/vsc";
import { FiHome } from "react-icons/fi";
import { Link } from "react-router-dom";
import logo from "../../assets/images/res-logo.png";
import Banner from "../Banner/Banner";
import SearchBar from "../SearchBar/SearchBar";
import { CartContext } from "../../context/CartContext/CartContextProvider";

function Header() {
  const [isSticky, setIsSticky] = useState(false);
  const { state } = useContext(CartContext);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 0) {
        setIsSticky(true);
      } else {
        setIsSticky(false);
      }
    };
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <>
      <nav
        className={`bg-gradient-to-r from-green-400 to-gray-800 ${
          isSticky ? "sticky top-0 z-50" : ""
        }`}
      >
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 flex items-center justify-between h-16">
          <div className="flex items-center">
            <img src={logo} className="h-10 sm:h-12 pl-2" alt="Logo" />
            <span className="text-white text-sm sm:text-2xl">
              فست فود
            </span>
          </div>
          <div className=" hidden sm:block">
          <SearchBar />
          </div>
          <div className="flex justify-center items-center">
            <Link to="/cart" className="text-white sm:p-3 relative p-2">
              <span className="absolute top-0 right-0 bg-red-500 text-white rounded-full px-2 py-1 text-xs">
                {state.itemsCounter}
              </span>
              <FiShoppingCart className=" w-5 h-5 sm:h-7 sm:w-7"/>
            </Link>
            <Link to="/login" className="text-white sm:p-3 p-2">
              <VscAccount  className=" w-5 h-5 sm:h-7 sm:w-7"/>
            </Link>
            <Link to="/" className="text-white sm:p-3 p-2">
              <FiHome className=" w-5 h-5 sm:h-7 sm:w-7" />
            </Link>
          </div>
        </div>
      </nav>
      <Banner />
    </>
  );
}

export default Header;
