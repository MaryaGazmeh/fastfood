import React from "react";
import "./banner.css";


const Banner = () => {
  return (
    <>
      <header className=" md:h-auto h-4 p-10 w-auto bg-cover">
        <div>
          <h1 className=" text-sm sm:text-2xl text-red-800 text-center font-extrabold">
            بهترین فست فودها را از ما بخواهید!
          </h1>
        </div>
      </header>
     
      
    </>
  );
};

export default Banner;
