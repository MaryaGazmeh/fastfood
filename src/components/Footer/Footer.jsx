import React from "react";

const Footer = () => {
  return (
    <footer className="bg-gray-800 text-white py-8">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 grid grid-cols-1 sm:grid-cols-2  md:grid-cols-4 gap-4">
        <div className="sm:text-lg text-sm mb-4 md:mb-0">
          <h3 className="font-bold mb-2">فست فود</h3>
          <p>اولین نیستیم، ولی بهترینیم!</p>
        </div>
        <div className="sm:text-lg text-sm mb-4 md:mb-0">
          <h3 className="font-bold mb-2">زمان های تحویل</h3>
          <div>
            <span>یکشنبه تا پنجشنبه</span>
            <p>10:00am - 11:00pm</p>
          </div>
          <div>
            <span>شنبه ها</span>
            <p>تعطیل</p>
          </div>
        </div>
        <div className="sm:text-lg text-sm mb-4 md:mb-0">
          <h3 className="font-bold mb-2">ارتباط</h3>
          <p>ارتباط با ما : تهران </p>
          <p>تلفن : 0123456789</p>
          <p>ایمیل : example@gmaol.com</p>
        </div>
        <div className=" sm:text-lg text-sm ">
          <h3 className="font-bold mb-2">خبرنامه</h3>
          <p className="mb-4">از جدیدترین محصولات ما باخبر شوید!</p>
          <div className="flex flex-col">
            <input
              type="email"
              placeholder="ایمیل"
              className="bg-gray-700 text-white px-3 py-2 rounded-md mb-2"
            />
            <button className="bg-blue-500 text-white px-4 py-2 rounded-md">
              مشترک شدن
            </button>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
