import React from "react";
import CardItem from "../../Shared/Card";

const Product = ({ product }) => {
  return <CardItem product={product} />;
};

export default Product;
