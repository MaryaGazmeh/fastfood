import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import { BASE_URL } from "../../Utils/baseApi";
import Loading from "../../Shared/Loading";
import { productContext } from "../../context/ProductContext/ContextProvider";
import SearchBar from "../SearchBar/SearchBar";

const CategoryProduct = () => {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);
  const [index, setIndex] = useState(-1);
  const { filterItem } = useContext(productContext);

  useEffect(() => {
    const fetchCategories = async () => {
      const response = await axios.get(`${BASE_URL}/FoodCategory/categories`);
      setCategories(response.data);
      setLoading(false);
    };
    fetchCategories();
  }, []);

  const renderContent = () => {
    return (
      <ul className="flex justify-start md:gap-2 w-auto border bg-white border-green-700 rounded-md shadow-lg">
        <li className="p-2 md:p-3">
          <Link
            className={`${index === -1 ? "text-red-700" : ""} text-xs md:text-sm`}
            onClick={() => {
              setIndex(-1);
              filterItem();
            }}
          >
            همه فست فودها
          </Link>
        </li>
        {categories.map((category, curentIndex) => (
          <li key={category.id} className="p-1 text-xs md:text-sm md:p-3">
            <Link
              onClick={() => {
                filterItem(category.id);
                setIndex(curentIndex);
              }}
              className={`${index === curentIndex ? "text-red-700" : ""}`}
            >
              {category.name}
            </Link>
          </li>
        ))}
      </ul>
    );
  };

  return loading ? (
    <div className="flex justify-center items-center">
      <Loading />
  </div>
  ) : (
    <div className="flex flex-col items-center">
     
      {renderContent()}
    </div>
  );
};

export default CategoryProduct;
