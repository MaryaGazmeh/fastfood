import { useContext, useState } from "react";
import { BsSearch } from "react-icons/bs";
import { productContext } from "../../context/ProductContext/ContextProvider";
const SearchBar = () => {
  const [value, setValue] = useState("");
  const { serachItems } = useContext(productContext);

  const onSubmit = (e) => {
    e.preventDefault();
    serachItems(value);
  };

  return (
    <form onSubmit={onSubmit}>
      <div className=" flex items-center relative">
        <input
          className="rounded m-1 p-2 border border-green-400 focus:outline-none "
          type="text"
          placeholder="جستجوی فست فود ..."
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />

        <BsSearch className=" absolute left-4" />
      </div>
    </form>
  );
};

export default SearchBar;
