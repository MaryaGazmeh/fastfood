import React, { createContext, useState, useEffect } from "react";
import { BASE_URL } from "../../Utils/baseApi";
import axios from "axios";

export const productContext = createContext();

const ContextProvider = ({ children }) => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchApi = async (categoryId) => {
    const response = await axios.get(
      `${BASE_URL}/FastFood/list/${
        categoryId ? "?categoryId=" + categoryId : ""
      }`
    );
    setProducts(response.data);
    setLoading(false);
  };

  useEffect(() => {
    fetchApi();
  }, []);

  //For foods filtered by category
  const filterItem = (categoryId) => {
    fetchApi(categoryId);
  };

  //For result of search
  const serachItems = async (term) => {
    const response = await axios.get(
      `${BASE_URL}/FastFood/search/${term ? "?term=" + term : ""}`
    );
    setProducts(response.data);
  };

  return (
    <productContext.Provider value={{ products, filterItem, serachItems }}>
      {loading ? (
        <div className="flex space-x-2 justify-center items-center bg-white h-screen dark:invert">
          <span className="sr-only">Loading...</span>
          <div className="h-8 w-8 bg-blue-500 rounded-full animate-bounce [animation-delay:-0.3s]"></div>
          <div className="h-8 w-8 bg-blue-500 rounded-full animate-bounce [animation-delay:-0.15s]"></div>
          <div className="h-8 w-8 bg-blue-500 rounded-full animate-bounce"></div>
        </div>
      ) : (
        children
      )}
    </productContext.Provider>
  );
};

export default ContextProvider;
