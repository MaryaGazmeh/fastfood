import React, { useContext } from "react";
import { Link, useParams } from "react-router-dom";
import { productContext } from "../context/ProductContext/ContextProvider";
import { CartContext } from "../context/CartContext/CartContextProvider";
import { quantity, isCart } from "../Utils/util";

const ProductDetail = () => {
  const { state, dispatch } = useContext(CartContext);
  const { id } = useParams();
  const { products } = useContext(productContext);
  const product = products.find((item) => item.id === parseInt(id));
  const { name, imageUrl, price, ingredients } = product;

  if (!products) {
    return <div>محصولی یافت نشد</div>;
  }

  return (
    <>
      <div className="flex flex-col md:flex-row justify-around items-center my-8">
        <div className="md:mr-8 mb-4 mr-2 md:mb-0 max-w-md">
          <img src={imageUrl} className="w-full rounded-lg" alt={name} />
        </div>
        <div className="max-w-md">
          <div className="mb-4">
            <h2 className="text-base md:text-xl lg:text-2xl font-bold">{name}</h2>
            <p className="text-gray-600 text-base md:text-xl ">قیمت: {price.toLocaleString()}</p>
            <h3 className=" text-base md:text-xl lg:text-2xl font-semibold">ترکیبات:</h3>
            <p className="text-gray-600 text-base md:text-xl">{ingredients}</p>
          </div>
          <div className="mb-4">
            {isCart(state, product.id) ? (
              <div className="inline-flex items-center mt-4">
              <button
                onClick={() => dispatch({ type: "INCREASE", payload: product })}
                className="bg-white rounded-r border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
              >
                {" "}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-4"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  {" "}
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M12 4v16m8-8H4"
                  />{" "}
                </svg>{" "}
              </button>
              <div className="bg-gray-100 border-t border-b border-gray-100 text-gray-600 hover:bg-gray-100 inline-flex items-center px-4 py-1 select-none">
                {" "}
                {quantity(state, product.id)}
              </div>{" "}
              {/* Minus button */}
              {quantity(state, product.id) > 1 && (
                <button
                  onClick={() => dispatch({ type: "DECREASE", payload: product })}
                  className="bg-white rounded-l border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-4"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    {" "}
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M20 12H4"
                    />{" "}
                  </svg>{" "}
                </button>
              )}
              {quantity(state, product.id) === 1 && (
                <button
                  onClick={() =>
                    dispatch({ type: "REMOVE_ITEM", payload: product })
                  }
                  className="bg-white rounded-l border text-gray-600 hover:bg-gray-100 active:bg-gray-200 disabled:opacity-50 inline-flex items-center px-2 py-1 border-r border-gray-200"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-4"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    {" "}
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M20 12H4"
                    />{" "}
                  </svg>{" "}
                </button>
              )}
            </div>
            ) : (
              <Link
                onClick={() => {
                  dispatch({ type: "ADD_ITEM", payload: product });
                }}
                className=" bg-green-500 hover:bg-green-600 rounded-md text-white md:p-2 p-1 md:mr-2 text-xs sm:text-sm"
              >
                افزودن به سبد خرید
              </Link>
            )}

          <div className=" flex items-center mt-7 mr-0">
          <Link
              to="/"
              className="bg-blue-500 hover:bg-blue-600 rounded-md text-white md:mt- md:p-2 p-1 md:mr-2 mr-1 text-xs sm:text-sm"
            >
              بازگشت به فروشگاه
            </Link>
          </div>
          </div>
        </div>
      </div>
      <hr className="w-auto mb-10" />
    </>
  );
};

export default ProductDetail;
