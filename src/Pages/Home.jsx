import React, { useContext, useEffect } from "react";
import { productContext } from "../context/ProductContext/ContextProvider";
import CategoryProduct from "../components/CategoryProduct/CategoryProduct";
import AllProducts from "../components/Allproducts/AllProducts";
import NoutFound from "../components/NotFound/NotFound";

const Home = () => {
  const { products, filterItem } = useContext(productContext);
  useEffect(() => {
    filterItem();
  }, []);

  const renderContent = () => {
    if (products.length === 0) {
      return <NoutFound />;
    }

    return <AllProducts />;
  };

  return (
    <>
      <CategoryProduct />
      
      {renderContent()}
    </>
  );
};

export default Home;
