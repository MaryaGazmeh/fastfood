import React from "react";
import { Routes , Route , Navigate } from "react-router-dom";
import Home from '../Pages/Home.jsx'
import ProductDetail from '../Pages/ProductDetails.jsx'
import ShopCart from "../components/ShopCart/ShopCart.jsx";
import NotFound from '../components/NotFound/NotFound.jsx'
import Login from "../Pages/Login.jsx";

const Routers = () => {
  return <div className='px-10'>
    <Routes>
      <Route path="/" element = {<Navigate to='/home'/>}/>
      <Route path="/home" element = {<Home/>}/>
      <Route path="/products/:id" element = {<ProductDetail/>}/>
      <Route path="/cart" element = {<ShopCart/>}/>
      <Route path="/login" element = {<Login/>}/>
      <Route path="/*" element={ <NotFound/>} />
    </Routes>
  </div>;
};

export default Routers;
